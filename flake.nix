{
  description = "Description for the project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    flake-parts,
    treefmt-nix,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        treefmt-nix.flakeModule
      ];
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: {
        devShells.default = pkgs.mkShell {
          name = "st";
          packages = [
            config.treefmt.build.wrapper
          ];
          nativeBuildInputs = [
            pkgs.pkg-config
            pkgs.gnumake
            pkgs.fontconfig.dev
            pkgs.freetype.dev
          ];
          buildInputs = [
            pkgs.xorg.libX11.dev
            pkgs.xorg.libXft.dev
          ];
        };

        treefmt.config = {
          projectRootFile = "flake.nix";
          programs.clang-format.enable = true;
          programs.alejandra.enable = true;
          settings.formatter.alejandra.options = ["-q"];
        };
      };
      flake = {
      };
    };
}
